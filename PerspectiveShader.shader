﻿

Shader "Unlit/PerspectiveShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_GradientTex ("Gradient", 2D) = "white" {}
		_MainTint ("Main Tint", Color) = (1, 1, 1, 1)
		_AdditiveTint ("Main Tint", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType"="Plane"
		}
		Cull Off
		Lighting Off
		ZWrite Off
		ZTest Off
		Fog { Mode Off }
		 

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha 
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 uvq : TEXCOORD0;
                float4 color    : COLOR;
			};

			struct v2f
			{
				float4 uvq : TEXCOORD0;
				float4 vertex : SV_POSITION;
                fixed4 color    : COLOR;
			};

			sampler2D _MainTex;
			sampler2D _GradientTex;
			float4 _MainTex_ST;
			float4 _MainTint; 
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uvq = v.uvq;
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float2 uv = i.uvq.xy / i.uvq.z + _MainTex_ST.zw;
				fixed4 col = tex2D(_MainTex, uv) * _MainTint;
				fixed4 gradientCol = tex2D(_GradientTex, uv);
				col *= gradientCol;
				col.a *= i.color.a;
				return col;
			}
			ENDCG
		}

		Pass
		{
			Blend SrcAlpha One
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 uvq : TEXCOORD0;
                float4 color    : COLOR;
			};

			struct v2f
			{
				float4 uvq : TEXCOORD0;
				float4 vertex : SV_POSITION;
                fixed4 color    : COLOR;
			};

			sampler2D _MainTex;
			sampler2D _GradientTex;
			float4 _MainTex_ST;
			float4 _AdditiveTint; 
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uvq = v.uvq;
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				float2 uv = i.uvq.xy / i.uvq.z + _MainTex_ST.zw;
				fixed4 col = tex2D(_MainTex, uv) * _AdditiveTint;
				fixed4 gradientCol = tex2D(_GradientTex, uv);
				col *= gradientCol;
				col.a *= i.color.a;
				return col;
			}
			ENDCG
		}

	}
}
