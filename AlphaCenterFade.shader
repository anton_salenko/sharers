﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UI/AlphaCenterFade"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_FadePower ("Fade Power", Range(0, 2)) = 1
		_FadeRadius ("fade out Radius", Range(0.01, 1)) = 1
		_FadeInRadius ("fade in Radius", Range(0.01, 1)) = 1

		_BlendingType ("Blending Type", Range (0, 1)) = 0

		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		GrabPass
        {
            "_BackgroundTexture"
        }
		
		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}

		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		
		ColorMask [_ColorMask]



		Pass
		{	
			Blend One Zero
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 grabPos	: TEXCOORD1;
			};
			
			fixed4 _Color;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
#ifdef UNITY_HALF_TEXEL_OFFSET
				OUT.vertex.xy += (_ScreenParams.zw-1.0)*float2(-1,1);
#endif
				OUT.color = IN.color * _Color;
				OUT.grabPos = ComputeGrabScreenPos(OUT.vertex);
				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _BackgroundTexture;
			float _FadePower;
			float _FadeRadius;
			float _FadeInRadius;
			float _BlendingType;

			fixed4 frag(v2f IN) : SV_Target
			{
				half4 color = tex2D(_MainTex, IN.texcoord) * IN.color;
				half4 bg_color = tex2Dproj(_BackgroundTexture, IN.grabPos);
				color.a -= _FadePower * (1 - saturate((distance(float2(0.5, 0.5), IN.texcoord) - _FadeInRadius) / (_FadeRadius)));
				color.a = saturate(color.a);
				clip (color.a - 0.01);

				float3 screen_blending = lerp(bg_color.rgb, float3(1, 1, 1) - (float3(1, 1, 1) - color.rgb) * (float3(1, 1, 1) - bg_color.rgb), color.a);
				float3 alpha_blending = lerp(bg_color.rgb, color.rgb, color.a);
				color.rgb = lerp(screen_blending, alpha_blending, _BlendingType);
				color.a = 1;
				return color;
			}
		ENDCG
		}
	}
}